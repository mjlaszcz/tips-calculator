﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TipsCalculator.Data.Entitites
{
    public class ExchangeRateEntity 
    {
        private string _currencyFrom;
        private string _currencyTo;

        public int Id { get; set; }
        [Required]
        public string FromCurrency
        {
            get { return _currencyFrom; }
            set { _currencyFrom = value.ToUpper(); }
        }

        [Required]
        public string ToCurrency
        {
            get { return _currencyTo; }
            set { _currencyTo = value.ToUpper(); }
        }
        
        [Required]
        public decimal Rate { get; set; }

    }
}
