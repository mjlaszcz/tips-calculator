﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TipsCalculator.Data.Entitites
{
    public class TotalBillEntity
    {
        private string _currency;
        private decimal _amount;
        
        public int Id { get; set; }
        public string Sku { get; set; }
        public decimal TipPercentage { get; set; }
        public decimal Amount
        {
            get { return _amount;}
            set { _amount = decimal.Round(value, 2);}
        }
        //public decimal TipAmount { get; set; }
        public string Currency {
            get { return _currency; }
            set { _currency = value.ToUpper(); }
        }
        public ICollection<BillEntity> Bills { get; set; }
    }
}
