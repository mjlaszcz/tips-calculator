﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TipsCalculator.Data.Entitites
{
    public class BillEntity
    {
        private decimal _amount;
        private decimal _tip;
        private string _currency;

        public int Id { get; set; }
        public string Sku { get; set; }
        [Required]
        public decimal Amount
        {
            get { return _amount; }
            set { _amount = decimal.Round(value, 2); }
        }
        public decimal Tip {
            get { return _tip; }
            set { _tip = decimal.Round(value, 2); }
        }
        [Required]
        public string Currency
        {
            get { return _currency; }
            set { _currency = value.ToUpper(); }
        }
    }
}
