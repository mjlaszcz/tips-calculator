﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TipsCalculator.Data.Entitites;

namespace TipsCalculator.Data.Interfaces
{
    interface IGetData
    {
        Task<ICollection<ExchangeRateEntity>> GetExchangeRatesFrom3rdPartyServiceAsync();

        bool SaveExchangeRates(ICollection<ExchangeRateEntity> rates);
        bool UpdateExchangeRates(ICollection<ExchangeRateEntity> newRates);


        bool ExchangeRatesExist();

        Task<ICollection<BillEntity>> GetBillAsync();

        ICollection<TotalBillEntity> ApplyTipToBills(ICollection<BillEntity> bills, int? percentage, string toCurrency);
        bool RemoveTotalTipsResults();
    }
}
