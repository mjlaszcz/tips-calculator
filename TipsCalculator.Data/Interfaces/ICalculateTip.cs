﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TipsCalculator.Data.Entitites;

namespace TipsCalculator.Data.Interfaces
{
    public interface ICalculateTip
    {
        Task<ICollection<TotalBillEntity>> GetCalculatedTipAsync();
    }
}
