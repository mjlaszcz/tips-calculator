﻿using TipsCalculator.Data.Interfaces;

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TipsCalculator.Data.Entitites;
using TipsCalculator.Data.Data;
using System.Net.Http;
using System.Net.Http.Headers;
using TipsCalculator.Data.Helpers;
using TipsCalculator.Data.Helpers.CustomExceptions;
using System.Runtime.CompilerServices;

//#if DEBUG

//[assembly: InternalsVisibleToAttribute("TipsCalculator.Test")] 
//#endif

namespace TipsCalculator.Data.Repositories
{
    public class CalculateTipsRepository : IGetData, ICalculateTip
    {
        private readonly TipsCalculatorContext _ctx;

        public CalculateTipsRepository(TipsCalculatorContext ctx)
        {
            _ctx = ctx;
        }

        #region Calculate Tip
        public async Task<ICollection<TotalBillEntity>> GetCalculatedTipAsync()
        {
            ICollection<TotalBillEntity> res = new List<TotalBillEntity>();
            ICollection<BillEntity> bills = new List<BillEntity>();
            try
            {
                bills = await GetBillAsync();
                var exchangeRates = await GetExchangeRatesFrom3rdPartyServiceAsync();

                if (!ExchangeRatesExist())
                    SaveExchangeRates(exchangeRates);
                else
                    UpdateExchangeRates(exchangeRates);
            }
            catch (ServiceNotFoundException)
            {
                // TODO : maybe log or warning for users ?
            }
            catch (RatioDoesNotExistException)
            {
                // TODO : maybe log or warning for users ?
            }
            catch (Exception)
            {
                throw;
            }

            // if exchange rates exist
            // else null
            if (_ctx.ExchangeRates.Any() && bills.Count > 0)
                return ApplyTipToBills(bills);
            else
                RemoveTotalTipsResults();

            return res;
        }

        #endregion

        #region Get Data

        public bool ExchangeRatesExist()
        {
            if (_ctx != null)
                return _ctx.ExchangeRates.Any();
            else
                return false;
        }

        public async Task<ICollection<BillEntity>> GetBillAsync()
        {
            string baseAddress = "http://quiet-stone-2094.herokuapp.com/";
            string method = "transactions.json";
            string requestHeaders = "application/json";

            List<BillEntity> results = new List<BillEntity>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(requestHeaders));

                // HTTP GET
                HttpResponseMessage response = await client.GetAsync(method);
                if (response.IsSuccessStatusCode)
                {
                    ExchangeRateEntity sr = new ExchangeRateEntity();
                    var json = await response.Content.ReadAsStringAsync();
                    // Names do not correspond so we use dynamic 
                    //results.AddRange(JsonConvert.DeserializeObject<List<SustitutionRule>>(json));
                    results.AddRange(GeneralHelper.ConvertStringToBillEntity(json));
                    return results;
                }
                else
                {
                    throw new ServiceNotFoundException();
                }

            }
        }
        
        public async Task<ICollection<ExchangeRateEntity>> GetExchangeRatesFrom3rdPartyServiceAsync()
        {
            string baseAddress = "http://quiet-stone-2094.herokuapp.com/";
            string method = "rates.json";
            string requestHeaders = "application/json";

            List<ExchangeRateEntity> results = new List<ExchangeRateEntity>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(requestHeaders));

                // HTTP GET
                HttpResponseMessage response = await client.GetAsync(method);
                if (response.IsSuccessStatusCode)
                {
                    ExchangeRateEntity sr = new ExchangeRateEntity();
                    var json = await response.Content.ReadAsStringAsync();
                    // Names do not correspond so we use dynamic 
                    //results.AddRange(JsonConvert.DeserializeObject<List<SustitutionRule>>(json));
                    results.AddRange(GeneralHelper.ConvertStringToExchangeRateEntity(json));
                    return results;
                }
                else
                {
                    throw new ServiceNotFoundException();
                }

            }
        }
        
        public bool SaveExchangeRates(ICollection<ExchangeRateEntity> rates)
        {
            if (rates.Count > 0)
            {
                _ctx.ExchangeRates.AddRange(rates);
                return _ctx.SaveChanges() > 0;
            }
            else
                return false;
        }

        public bool UpdateExchangeRates(ICollection<ExchangeRateEntity> newRates)
        {
            // remove old rates
            if (ExchangeRatesExist())
            {
                if (RemoveExchangeRates())
                {
                    _ctx.ExchangeRates.AddRange(newRates);
                    return _ctx.SaveChanges() > 0;
                } 
            }
            return false;
               
        }

        public bool RemoveExchangeRates()
        {
            if(_ctx.ExchangeRates != null && _ctx.ExchangeRates.Count() > 0)
            {
                foreach(var entity in _ctx.ExchangeRates)
                _ctx.Entry(entity).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                return _ctx.SaveChanges() > 0; 
            }
            return false;
        }

        public bool RemoveTotalTipsResults()
        {
            if (_ctx.TotalBills != null && _ctx.TotalBills.Count() > 0)
            {
                foreach (var entity in _ctx.TotalBills)
                    _ctx.TotalBills.Remove(entity);
                return _ctx.SaveChanges() > 1; 
            }
            return false;
        }


        /// <summary>
        /// By default we will convert total tip and bills to eur and tip will be 5%
        /// </summary>
        /// <param name="percentage"></param>
        /// <param name="toCurrecny"></param>
        /// <returns></returns>
        public ICollection<TotalBillEntity> ApplyTipToBills(ICollection<BillEntity> bills, int? percentage = 5, string toCurrecny = "EUR")
        {
            List<TotalBillEntity> totalBills = new List<TotalBillEntity>();
            Dictionary<string, decimal> RatioTable = new Dictionary<string, decimal>()
            {
                { toCurrecny, 1 }
            };
            try
            {
                var billsGroupsBySku = bills.GroupBy(x => x.Sku).ToList();
                // Grouped By SKU
                foreach (var billList in billsGroupsBySku)
                {
                    decimal totalAmount = 0;
                    foreach (var billListCurrency in billList.GroupBy(x=> x.Currency).ToList())
                    {
                        decimal ratio = 1;
                        if (!RatioTable.ContainsKey(billListCurrency.First().Currency))
                        {
                            ratio = GeneralHelper.ExchangeRatio(_ctx.ExchangeRates.ToList(), billListCurrency.First().Currency, toCurrecny);
                            RatioTable.Add(billListCurrency.First().Currency, ratio);
                        }
                        else
                        {
                            ratio = RatioTable[billListCurrency.First().Currency];
                        }
                        // calculate tip in every bill
                        foreach (var bill in billListCurrency)
                        {
                            totalAmount += bill.Amount * ratio;
                            bill.Tip = ( bill.Amount * (decimal)percentage / 100);
                        }  
                    }
                    // add total bill for bills grouped by SKU
                    totalBills.Add(new TotalBillEntity
                    {
                        Sku = billList.First().Sku,
                        Bills = billList.ToList(),
                        Amount = totalAmount,
                        TipPercentage = (decimal)(percentage / 100),
                        Currency = toCurrecny
                    });
                }
            }
            catch (Exception)
            {
            }
            return totalBills;
        }


        #endregion
    }
}
