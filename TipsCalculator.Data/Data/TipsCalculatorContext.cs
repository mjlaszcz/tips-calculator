﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TipsCalculator.Data.Entitites;
using JetBrains.Annotations;

namespace TipsCalculator.Data.Data
{
    public class TipsCalculatorContext : DbContext
    {
        public TipsCalculatorContext(DbContextOptions options)
            :base(options)
        {
                
        }

        public DbSet<BillEntity> Bills { get; set; }
        public DbSet<ExchangeRateEntity> ExchangeRates { get; set; }
        public DbSet<TotalBillEntity> TotalBills { get; set; }
    }
}
