﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TipsCalculator.Data.Helpers.CustomExceptions
{
    public class ServiceNotFoundException : System.Exception
    {
        public ServiceNotFoundException()
        {
        }

        public ServiceNotFoundException(string message) : base(message)
        {
        }
    }
}
