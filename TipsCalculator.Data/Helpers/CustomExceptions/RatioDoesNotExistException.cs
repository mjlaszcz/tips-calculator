﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TipsCalculator.Data.Helpers.CustomExceptions
{
    public class RatioDoesNotExistException : System.Exception
    {
        public RatioDoesNotExistException()
        {
        }

        public RatioDoesNotExistException(string message) : base(message)
        {
        }
    }
}
