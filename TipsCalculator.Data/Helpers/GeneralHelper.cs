﻿using System;
using System.Collections.Generic;
using System.Text;
using TipsCalculator.Data.Entitites;
using System.Linq;
using Newtonsoft.Json;
using System.Runtime.CompilerServices;
using TipsCalculator.Data.Helpers.CustomExceptions;

#if DEBUG
// we make internal methods visible
[assembly: InternalsVisibleTo("TipsCalculator.Test")] 
#endif

namespace TipsCalculator.Data.Helpers
{
    public class GeneralHelper
    {
        internal static decimal CalculateTip(decimal amount, decimal percentage = 5)
        {
            return amount * percentage / 100;
        }

        internal static bool TableContainsExchangeRates(ICollection<ExchangeRateEntity> rates, string from, string to)
        {
            // If we have conversion units
            if (rates.Where(x => x.FromCurrency.Contains(from.ToUpper())).Any()
                && rates.Where(x => x.ToCurrency.Contains(to.ToUpper())).Any())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        internal static decimal ExchangeRatio(ICollection<ExchangeRateEntity> rates, string from, string to)
        {
            
            decimal ratio = 0;
            if (rates.Where(x => x.FromCurrency.Contains(from.ToUpper()) && x.ToCurrency.Contains(to.ToUpper())).Any())
            {
                return rates.Where(x => x.FromCurrency.Contains(from.ToUpper()) && x.ToCurrency.Contains(to.ToUpper()))
                    .Select(y => y.Rate).First();
            }
            else
            {
                foreach (var rate in rates.Where(x => x.FromCurrency.Contains(from.ToUpper())).ToList())
                {
                    ratio = ExchangeRatio(rates, rate.ToCurrency.ToUpper(), to.ToUpper(), rate.Rate);
                    if (ratio != 0)
                        break;
                }
                if(ratio != 0)
                    return ratio;
                else
                    throw new RatioDoesNotExistException();
            }
           
        }

        private static decimal ExchangeRatio(ICollection<ExchangeRateEntity> rates, string from, string to, decimal exchangeRatio = 1)
        {
            if (exchangeRatio != 0)
            {
                decimal ratio = 0;
                if (rates.Where(x => x.FromCurrency.Contains(from.ToUpper()) && x.ToCurrency.Contains(to.ToUpper())).Any())
                {
                    return rates.Where(x => x.FromCurrency.Contains(from.ToUpper()) && x.ToCurrency.Contains(to.ToUpper()))
                        .Select(y => y.Rate).First()
                        * exchangeRatio;
                }
                else
                {
                    foreach (var rate in rates.Where(x => x.FromCurrency.Contains(from.ToUpper())).ToList())
                    {
                        ratio = ExchangeRatio(rates, rate.ToCurrency.ToUpper(), to.ToUpper(), exchangeRatio * rate.Rate);
                        if (ratio != 0)
                            break;
                    }
                    return ratio;
                }
            }
            else
            {
                return 0;
            }
        }

        internal static ICollection<ExchangeRateEntity> ConvertStringToExchangeRateEntity(string json)
        {
            try
            {
                List<ExchangeRateEntity> resultList = new List<ExchangeRateEntity>();
                dynamic dyn = JsonConvert.DeserializeObject(json);
                foreach (var obj in dyn)
                {
                    ExchangeRateEntity r = new ExchangeRateEntity()
                    {
                        FromCurrency = obj.from.ToString(),
                        ToCurrency = obj.to.ToString(),
                        Rate = (decimal)obj.rate
                    };
                    // See if currency are diffrent
                    if(!r.FromCurrency.Equals(r.ToCurrency))
                        resultList.Add(r);
                }
                return resultList;
            }
            catch (Exception)
            {
                throw new FormatException();
            }
        }

        internal static ICollection<BillEntity> ConvertStringToBillEntity(string json)
        {
            try
            {
                List<BillEntity> resultList = new List<BillEntity>();
                dynamic dyn = JsonConvert.DeserializeObject(json);
                foreach (var obj in dyn)
                {
                    resultList.Add(new BillEntity
                    {
                        Sku = obj.sku.ToString(),
                        Amount = decimal.Parse(String.Format("{0:0.00}",obj.amount)),
                        Currency = obj.currency.ToString()
                    });
                }
                return resultList;
            }
            catch (Exception)
            {
                throw new FormatException();
            }
        }
        // future update... check if rate exists
        //internal static ICollection<ExchangeRateEntity> UpdateExchangeRates(ICollection<ExchangeRateEntity> newRates, ICollection<ExchangeRateEntity> oldRates)
        //{
        //    ICollection<ExchangeRateEntity> res = new List<ExchangeRateEntity>();
        //    foreach (var rate in newRates)
        //    {
        //        // if the same currency link exist we update it's rate
        //        // else add it to ExchangeRates
        //        if (oldRates.Where(x => x.FromCurrency.Equals(rate.FromCurrency)
        //                                     && x.ToCurrency.Equals(rate.ToCurrency)).Any())
        //        {
        //            ExchangeRateEntity r = oldRates.Where(x => x.FromCurrency.Equals(rate.FromCurrency)
        //                                && x.ToCurrency.Equals(rate.ToCurrency)).
        //                                First();
        //            r.Rate = rate.Rate;
        //            res.Add(r);
        //        }
        //        else
        //        {
        //            res.Add(rate);
        //        }
        //    }
        //    return res;
        //}
    }
}
