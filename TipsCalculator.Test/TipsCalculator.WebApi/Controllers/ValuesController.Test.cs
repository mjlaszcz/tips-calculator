﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TipsCalculator.Data.Data;
using TipsCalculator.Data.Helpers.CustomExceptions;
using TipsCalculator.Data.Interfaces;
using TipsCalculator.Data.Repositories;
using TipsCalculator.WebApi.Controllers;

namespace TipsCalculator.Test.Controllers
{
    [TestClass]
    public class ValuesController_Test
    {
        DbContextOptionsBuilder _builder = null;
        TipsCalculatorContext _ctx = null;

        [TestMethod]
        public void Test_Controller_Ctor()
        {
            _builder = new DbContextOptionsBuilder<TipsCalculatorContext>().UseInMemoryDatabase("TipsCalculatorListCtor");
            _ctx = new TipsCalculatorContext(_builder.Options);
            var repo = new CalculateTipsRepository(_ctx);

            var testCtor = new ValuesController(_ctx, repo);
            Assert.IsInstanceOfType(testCtor, typeof(ValuesController));

        }

        [TestMethod]
        public void Test_Controller_Get()
        {
            _builder = new DbContextOptionsBuilder<TipsCalculatorContext>().UseInMemoryDatabase("TipsCalculatorListGet");
            _ctx = new TipsCalculatorContext(_builder.Options);
            var repo = new CalculateTipsRepository(_ctx);

            var testCtor = new ValuesController(_ctx, repo);
            Assert.IsInstanceOfType(testCtor, typeof(ValuesController));

            // we test untill we get a null resposne from 3rd party service we should get a ServiceNotFoundException                    
            Task.Run(async () =>
            {
                try
                {
                    var result = await testCtor.Get();
                    // there is a chance to thorw a ServiceNotFoundException
                    Assert.IsTrue(result is OkObjectResult || result is NoContentResult);
                }
                catch (ServiceNotFoundException)
                {
                    // custom exception
                    Assert.IsTrue(true);
                }
                catch (RatioDoesNotExistException)
                {
                    // custom exception
                    Assert.IsTrue(true);
                }
                catch (Exception)
                {
                    Assert.Fail();
                }
            }).GetAwaiter().GetResult();
            
        }
    }
}
