﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using TipsCalculator.Data.Data;
using TipsCalculator.Data.Entitites;
using TipsCalculator.Data.Helpers.CustomExceptions;
using TipsCalculator.Data.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;

namespace TipsCalculator.Test.Repositories
{
    [TestClass]
    public class CalculateTipsRepositoryTest
    {
        DbContextOptionsBuilder _builder = null;
        TipsCalculatorContext _ctx = null;

        [TestMethod]
        public void Test_Repo_GetCalculatedTip()
        {
            try
            {
                _builder = new DbContextOptionsBuilder<TipsCalculatorContext>().UseInMemoryDatabase("TipsCalculatorList01");
                _ctx = new TipsCalculatorContext(_builder.Options);
                var repo = new CalculateTipsRepository(_ctx);

                // Add Bill
                _ctx.Bills.Add(new BillEntity
                {
                    Amount = (decimal)120.2,
                    Currency = "usd",
                    Id = 1,
                    Sku = "EAS256",
                    Tip = (decimal)(120.2 * 0.05)
                });
                // Add exchange rate
                _ctx.ExchangeRates.Add(new ExchangeRateEntity
                {
                    FromCurrency = "usd",
                    Id = 1,
                    Rate = (decimal)1.2,
                    ToCurrency = "eur"
                });
                Assert.IsTrue(_ctx.SaveChanges() > 0);

                if (_ctx.ExchangeRates.Count() > 0 && _ctx.Bills.Count() > 0)
                {
                    ICollection<TotalBillEntity> result = null;

                    // we test untill we get a null resposne from 3rd party service we should get a ServiceNotFoundException                    
                    Task.Run(async () =>
                            {
                                try
                                {
                                    result = await repo.GetCalculatedTipAsync();
                                    // there is a chance to thorw a ServiceNotFoundException
                                    Assert.IsTrue(result != null && result.Count > 0);
                                }
                                catch (ServiceNotFoundException)
                                {
                                    // expected exception catched
                                    Assert.IsTrue(true);
                                }
                                catch (RatioDoesNotExistException)
                                {
                                    // custom exception
                                    Assert.IsTrue(true);
                                }
                            }).GetAwaiter().GetResult();

                }
                else
                    Assert.Fail();
            }
            catch (Exception)
            {
                Assert.Fail();
            }
            finally
            {
                // Dispose
                _ctx.Dispose();
            }
        }

        [TestMethod]
        public void Test_Repo_ExchangeRatesExist()
        {
            try
            {
                _builder = new DbContextOptionsBuilder<TipsCalculatorContext>().UseInMemoryDatabase("TipsCalculatorList02");
                _ctx = new TipsCalculatorContext(_builder.Options);
                var repo = new CalculateTipsRepository(_ctx);
             
                // Add exchange rate
                _ctx.ExchangeRates.Add(new ExchangeRateEntity
                {
                    FromCurrency = "usd",
                    Id = 1,
                    Rate = (decimal)1.2,
                    ToCurrency = "eur"
                });
                Assert.IsTrue(_ctx.SaveChanges() > 0);
                // ctx contains Exchange Rates
                Assert.IsTrue(repo.ExchangeRatesExist());
            }
            catch (Exception)
            {
                Assert.Fail();
            }
            finally
            {
                // Dispose
                _ctx.Dispose();
            }

        }

        [TestMethod]
        public void Test_Repo_GetBillAsync()
        {
            _builder = new DbContextOptionsBuilder<TipsCalculatorContext>().UseInMemoryDatabase("TipsCalculatorList03");
            _ctx = new TipsCalculatorContext(_builder.Options);
            var repo = new CalculateTipsRepository(_ctx);
            // GetBillAsync
            Task.Run(async () =>
            {
                try
                {
                    // resultas are icollection of BillEntity
                    // there is a chance to thorw a ServiceNotFoundException ... probably not...
                    // we trust no one
                    var result = await repo.GetBillAsync();
                    Assert.IsTrue(result.Count > 0);
                }
                catch (ServiceNotFoundException)
                {
                    // catch expected exception 
                    Assert.IsTrue(true);
                }
            }).GetAwaiter().GetResult();
            // Dispose
            _ctx.Dispose();
        }

        [TestMethod]
        public void Test_Repo_GetExchangeRatesFrom3rdPartyServiceAsync()
        {
            _builder = new DbContextOptionsBuilder<TipsCalculatorContext>().UseInMemoryDatabase("TipsCalculatorList04");
            _ctx = new TipsCalculatorContext(_builder.Options);
            var repo = new CalculateTipsRepository(_ctx);
            Task.Run(async () =>
            {
                try
                {
                    // resultas are icollection of ExchangeRateEntity
                    // there is a chance to thorw a ServiceNotFoundException 
                    // we know this service will fail from time to time
                    var result = await repo.GetExchangeRatesFrom3rdPartyServiceAsync();
                    Assert.IsTrue(result.Count > 0);
                }
                catch (ServiceNotFoundException)
                {
                    // catch expected exception 
                    Assert.IsTrue(true);
                }
            }).GetAwaiter().GetResult();
            // Dispose
            _ctx.Dispose();
        }

        [TestMethod]
        public void Test_Repo_SaveExchangeRates()
        {
            _builder = new DbContextOptionsBuilder<TipsCalculatorContext>().UseInMemoryDatabase("TipsCalculatorList05");
            _ctx = new TipsCalculatorContext(_builder.Options);
            var repo = new CalculateTipsRepository(_ctx);
            ICollection<ExchangeRateEntity> exchangeRateList = new List<ExchangeRateEntity>()
            {
                new ExchangeRateEntity
                {
                    FromCurrency = "usd",
                    Id = 1,
                    Rate = (decimal)1.2,
                    ToCurrency = "eur"
                },
                new ExchangeRateEntity
                {
                    FromCurrency = "eur",
                    Id = 2,
                    Rate = (decimal)1.22,
                    ToCurrency = "aud"
                }
            };            
            Assert.IsTrue(repo.SaveExchangeRates(exchangeRateList));
            // Dispose
            _ctx.Dispose();
        }

        [TestMethod]
        public void Test_Repo_UpdateExchangeRates()
        {
            _builder = new DbContextOptionsBuilder<TipsCalculatorContext>().UseInMemoryDatabase("TipsCalculatorListUpdate");
            _ctx = new TipsCalculatorContext(_builder.Options);
            var repo = new CalculateTipsRepository(_ctx);
            // add "old" exchange list
            ICollection<ExchangeRateEntity> exchangeRateList = new List<ExchangeRateEntity>()
            {
                new ExchangeRateEntity
                {
                    FromCurrency = "usd",
                    Id = 1,
                    Rate = (decimal)1.2,
                    ToCurrency = "eur"
                },
                new ExchangeRateEntity
                {
                    FromCurrency = "eur",
                    Id = 2,
                    Rate = (decimal)1.22,
                    ToCurrency = "aud"
                }
            };
            Assert.IsTrue(repo.SaveExchangeRates(exchangeRateList));
            // add "new" exchange rates
            ICollection<ExchangeRateEntity> exchangeRateListToUpdate = new List<ExchangeRateEntity>()
            {
                new ExchangeRateEntity
                {
                    FromCurrency = "aud",
                    Id = 1,
                    Rate = (decimal)2.2,
                    ToCurrency = "usd"
                },
                new ExchangeRateEntity
                {
                    FromCurrency = "eur",
                    Id = 2,
                    Rate = (decimal)0.22,
                    ToCurrency = "aud"
                }
            };

            for (int i = 0; i < 2; i++)
            {  
                Assert.IsTrue(repo.UpdateExchangeRates(exchangeRateListToUpdate));
                Assert.IsTrue(_ctx.ExchangeRates.Count() == 2);                
            }
            // Dispose
            _ctx.Dispose();
        }

        [TestMethod]
        public void Test_Repo_RemoveExchangeRates()
        {
            _builder = new DbContextOptionsBuilder<TipsCalculatorContext>().UseInMemoryDatabase("TipsCalculatorListRemove");
            _ctx = new TipsCalculatorContext(_builder.Options);
            var repo = new CalculateTipsRepository(_ctx);
            ICollection<ExchangeRateEntity> exchangeRateList = new List<ExchangeRateEntity>()
            {
                new ExchangeRateEntity
                {
                    FromCurrency = "usd",
                    Id = 1,
                    Rate = (decimal)1.2,
                    ToCurrency = "eur"
                },
                new ExchangeRateEntity
                {
                    FromCurrency = "eur",
                    Id = 2,
                    Rate = (decimal)1.22,
                    ToCurrency = "aud"
                }
            };
            Assert.IsTrue(repo.SaveExchangeRates(exchangeRateList));

            // remove exchange rates
            Assert.IsTrue(repo.RemoveExchangeRates());
            // exchange rates are null
            Assert.IsFalse(repo.RemoveExchangeRates());
            // Dispose
            _ctx.Dispose();
        }

        [TestMethod]
        public void Test_Repo_RemoveTotalTipsResults()
        {
            _builder = new DbContextOptionsBuilder<TipsCalculatorContext>().UseInMemoryDatabase("TipsCalculatorListRemoveTotalTips");
            _ctx = new TipsCalculatorContext(_builder.Options);
            var repo = new CalculateTipsRepository(_ctx);
            // Add new totalBillEntity
            _ctx.TotalBills.Add(new TotalBillEntity
            {
                Amount = (decimal)120.1,
                Bills = new List<BillEntity>()
                {
                    new BillEntity{Amount = (decimal)120.1, Currency ="eur", Id = 12, Sku = "ERES1234", Tip = (decimal)(120.1 * 00.5)}
                },
                Currency = "eur",
                Id = 22,
                Sku= "ERES1234",
                TipPercentage = 5
            });
            // save total bill entity
            Assert.IsTrue(_ctx.SaveChanges() > 0);
            //remove total bill entity
            Assert.IsTrue(repo.RemoveTotalTipsResults());
            // bill entity is null
            Assert.IsFalse(repo.RemoveTotalTipsResults());
            // Dispose
            _ctx.Dispose();
        }

        [TestMethod]
        public void Test_Repo_ApplyTipToBills()
        {
            _builder = new DbContextOptionsBuilder<TipsCalculatorContext>().UseInMemoryDatabase("TipsCalculatorListApply");
            _ctx = new TipsCalculatorContext(_builder.Options);
            var repo = new CalculateTipsRepository(_ctx);
            // Exchange Rate demo
            ICollection<ExchangeRateEntity> exchangeRateList = new List<ExchangeRateEntity>()
            {
                new ExchangeRateEntity
                {
                    FromCurrency = "usd",
                    Id = 11,
                    Rate = (decimal)1.2,
                    ToCurrency = "cad"
                },
                new ExchangeRateEntity
                {
                    FromCurrency = "cad",
                    Id = 22,
                    Rate = (decimal)2.1,
                    ToCurrency = "eur"
                }
            };
            // add exchange rates
            Assert.IsTrue(repo.SaveExchangeRates(exchangeRateList));
            Assert.IsTrue(_ctx.ExchangeRates.Count() == 2);

            // Bill demo
            ICollection<BillEntity> billEntityList = new List<BillEntity>
            {
                new BillEntity { Amount = (decimal)100.0023, Currency = "cad", Id = 101, Sku = "ERES1234", Tip = 0 },
                new BillEntity { Amount = (decimal)50.5044, Currency = "usd", Id = 102, Sku = "ERES1234", Tip = 0 },
                new BillEntity { Amount = (decimal)15.5673, Currency = "eur", Id = 103, Sku = "ERES1234", Tip = 0 }
            };

            /*
             * Exchange Rate
             *  cad -> eur : 2.1
             *  usd -> eur : 2.52
             *  
             *  Expected results are 1 total bill entity (eur) with 3 bills :
             *  Id 101 => 100   cad  ->    210 eur         ; tip : 5
             *  Id 102 => 50.5  usd  ->    127.26 eur      ; tip : 2.52
             *  ID 103 => 15.57 eur  ->    15.57 eur       ; tip : 0.78
             *  
             *  Total Sum = 352.83 eur
             */
            var result = repo.ApplyTipToBills(billEntityList);
            Assert.IsTrue(result.Count == 1 );
            Assert.IsTrue(result.First().Bills.Count == 3);
            Assert.IsTrue(result.First().Currency == "EUR");
            Assert.IsTrue(result.First().Amount == (decimal)352.83);
            // check tips
            Assert.IsTrue(result.First().Bills
                .Where(x => x.Id == 101)
                .Select(x => x.Tip).First() == 5);
            Assert.IsTrue(result.First().Bills
                .Where(x => x.Id == 102)
                .Select(x => x.Tip).First() == (decimal)2.52);
            Assert.IsTrue(result.First().Bills
                .Where(x => x.Id == 103)
                .Select(x => x.Tip).First() == (decimal)0.78);
            // Dispose
            _ctx.Dispose();
        }
    }
}
