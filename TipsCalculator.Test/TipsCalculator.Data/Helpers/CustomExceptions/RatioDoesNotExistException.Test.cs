﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace TipsCalculator.Test.TipsCalculator.Data.Helpers.CustomExceptions
{
    [TestClass]
    public class RatioDoesNotExistException
    {
        [TestMethod]
        public void Test_RatioDoesNotExistException()
        {
            Assert.IsInstanceOfType(new RatioDoesNotExistException(), typeof(RatioDoesNotExistException));
        }
    }
}
