﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using TipsCalculator.Data.Helpers.CustomExceptions;

namespace TipsCalculator.Test.Helpers.CustomExceptions
{
    [TestClass]
    public class ServiceNotFoundException_Test
    {
        [TestMethod]
        public void Test_ServiceNotFoundException()
        {
            Assert.IsInstanceOfType(new ServiceNotFoundException(), typeof(ServiceNotFoundException));
        }
    }
}
