﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using TipsCalculator.Data.Entitites;
using TipsCalculator.Data.Helpers;
using TipsCalculator.Data.Helpers.CustomExceptions;

namespace TipsCalculator.Test.Helpers
{
    [TestClass]
    public class GeneralHelper_Test
    {
        ICollection<ExchangeRateEntity> _testUnit = new List<ExchangeRateEntity>()
            {
                new ExchangeRateEntity{FromCurrency = "eur", Id = 1, Rate = (decimal)1.3, ToCurrency = "usd"},
                new ExchangeRateEntity{FromCurrency = "usd", Id = 1, Rate = (decimal)1.2, ToCurrency = "cad"},
                new ExchangeRateEntity{FromCurrency = "usd", Id = 1, Rate = (decimal)0.75, ToCurrency = "eur"}
            };
        string _jsonRate2 = "[{\"from\":\"USD\",\"to\":\"CAD\",\"rate\":\"0.61\"}" +
                            ",{\"from\":\"CAD\",\"to\":\"EUR\",\"rate\":\"1.04\"}]";

        string _jsonBill3 = "[{\"sku\":\"C9780\",\"amount\":\"21.6\",\"currency\":\"EUR\"}"
                            + ",{\"sku\":\"E4360\",\"amount\":\"32.6\",\"currency\":\"EUR\"}"
                            + ",{\"sku\":\"A0379\",\"amount\":\"28.6\",\"currency\":\"CAD\"}]";

        [TestMethod]
        public void Test_Helper_CalculateTip()
        {
            // by default tip % is 5%
            Assert.IsTrue(GeneralHelper.CalculateTip(100) == 5);
            Assert.IsTrue(GeneralHelper.CalculateTip(100, 1) == 1);
            Assert.IsFalse(GeneralHelper.CalculateTip(100, 20) == 1);
        }
        [TestMethod]
        public void Test_Helper_TableContainsExchangeRates()
        {
            Assert.IsTrue( GeneralHelper.TableContainsExchangeRates(_testUnit,"eur","usd"));
            Assert.IsTrue(GeneralHelper.TableContainsExchangeRates(_testUnit, "eur", "cad"));
            Assert.IsFalse(GeneralHelper.TableContainsExchangeRates(_testUnit, "eur", "aud"));
        }
        [TestMethod]
        public void Test_Helper_ExchangeRatio()
        {
            try
            {
                Assert.IsTrue( GeneralHelper.ExchangeRatio(_testUnit, "eur", "usd") == (decimal)1.3);
                Assert.IsTrue(GeneralHelper.ExchangeRatio(_testUnit, "eur", "cad") == (decimal)(1.3 * 1.2));            
                var test = GeneralHelper.ExchangeRatio(_testUnit, "aud", "cad");
                Assert.Fail();
            }
            catch (RatioDoesNotExistException)
            {
                Assert.IsTrue(true);
            }
        }
        [TestMethod]
        public void Test_Helper_ConvertStringToExchangeRateEntity()
        {
            Assert.IsTrue(GeneralHelper.ConvertStringToExchangeRateEntity(_jsonRate2).Count == 2);
        }
        [TestMethod]
        public void Test_Helper_ConvertStringToBillEntity()
        {
            Assert.IsTrue(GeneralHelper.ConvertStringToBillEntity(_jsonBill3).Count == 3);
        }
    }
}
