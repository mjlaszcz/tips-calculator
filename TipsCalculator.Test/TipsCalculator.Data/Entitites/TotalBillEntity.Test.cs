﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TipsCalculator.Data.Entitites;

namespace TipsCalculator.Test.Entitites
{
    [TestClass]
    public class TotalBillEntity_Test
    {
        TotalBillEntity totalBill = new TotalBillEntity()
        {
            Amount = (decimal)2555.8,
            Currency = "usd",
            Sku = "XEAS2",
            Id = 1,
            TipPercentage = 5,
            Bills = new List<BillEntity>
                {
                    new BillEntity
                    {
                        Amount = (decimal) 123.12,
                        Id = 1,
                        Sku = "XEAS2",
                        Currency = "usd",
                        Tip = (decimal)(123.12 * 0.05)
                    },
                    new BillEntity
                    {
                        Amount = (decimal) 2432.68,
                        Id = 2,
                        Sku = "XEAS2",
                        Currency = "usd",
                        Tip = (decimal)(2432.68 * 0.05)
                    }
                }
        };

        [TestMethod]
        public void Test_TotalBillEntity()
        {
            if (!totalBill.Currency.Equals("USD"))
                Assert.Fail();
            // we are using usd in every bill
            if (totalBill.Bills.Select(x => x.Amount).ToList().Sum() != totalBill.Amount)
                Assert.Fail();
            foreach(var bill in totalBill.Bills)
            {
                if(!bill.Sku.Equals(totalBill.Sku))
                    Assert.Fail();
            }
        }
    }
}
