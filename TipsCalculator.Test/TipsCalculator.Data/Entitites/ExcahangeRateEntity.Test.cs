﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using TipsCalculator.Data.Entitites;

namespace TipsCalculator.Test.Entitites
{
    [TestClass]
    public class ExcahangeRateEntity_Test
    {
        [TestMethod]
        public void Test_ExcahangeRateEntity()
        {
            ExchangeRateEntity rate = new ExchangeRateEntity
            {
				FromCurrency = "usd",
				ToCurrency = "eur",
				Id = 1,
				Rate = (decimal)1.322
            };

			if(rate.Id != 1 && rate.Rate != (decimal)1.322)
            {
                Assert.Fail();
            }

            if (!rate.ToCurrency.Equals("EUR") && !rate.FromCurrency.Equals("USD"))
                Assert.Fail();
        }
    }
}
