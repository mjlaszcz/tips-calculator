﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using TipsCalculator.Data.Entitites;

namespace TipsCalculator.Test.Entitites
{
    [TestClass]
    public class BillEntity_Test
    {
        [TestMethod]
        public void Test_BillEntity()
        {
            BillEntity bill = new BillEntity();
            Assert.IsInstanceOfType(bill, typeof(BillEntity));
        }
    }
}
