﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TipsCalculator.Data.Data;
using System.Linq;

namespace TipsCalculator.Test.Data
{
    [TestClass]
    public class TipsCalculatorContextTest
    {
        private DbContextOptions<TipsCalculatorContext> options;

        [TestMethod]
        public void Test_TipsCalculatorContext()
        {
            var builder = new DbContextOptionsBuilder<TipsCalculatorContext>().UseInMemoryDatabase(); 

            TipsCalculatorContext ctx = new TipsCalculatorContext(builder.Options);

            Assert.IsInstanceOfType(ctx, typeof(TipsCalculatorContext));
            Assert.IsTrue(ctx.ExchangeRates.Count() == 0 && ctx.Bills.Count() == 0);
            // Dispose
            ctx.Dispose();
        }
        
    }
}






