﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TipsCalculator.Data.Data;
using TipsCalculator.Data.Entitites;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Text;

namespace TipsCalculator.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        public readonly TipsCalculatorContext _ctx;

        private Data.Interfaces.ICalculateTip _repo;

        public ValuesController(TipsCalculatorContext ctx, Data.Interfaces.ICalculateTip repo)
        {
            _ctx = ctx;
            _ctx.SaveChanges();
            _repo = repo;
        }

        // GET api/values
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            //Data.Interfaces.ICalculateTip repo = null;
            try
            {
                ICollection<TotalBillEntity> result = await _repo.GetCalculatedTipAsync();
                if (result.Count > 0)
                {
                    var finalResult = result.Select(billList => new
                    {
                        sku = billList.Sku,
                        amount = billList.Amount,
                        tip = billList.TipPercentage,
                        currency = billList.Currency,
                        bills = billList.Bills.Select(x => new
                        {
                            sku = x.Sku,
                            amount = x.Amount,
                            tip = x.Tip,
                            currency = x.Currency
                        })
                    });
                    return Ok(finalResult);
                }
            }
            catch (Exception)
            {
                // TODO control exception
                // our dll throws 2 custom exceptions : ServiceNotFoundException  & RatioDoesNotExistException
            }
            return NoContent();

        }
    }
}
