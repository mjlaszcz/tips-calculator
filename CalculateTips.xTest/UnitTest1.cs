using System;
using TipsCalculator.Data.Data;
using Xunit;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace CalculateTips.xTest
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var builder = new DbContextOptionsBuilder<TipsCalculatorContext>().UseInMemoryDatabase();

            TipsCalculatorContext ctx = new TipsCalculatorContext(builder.Options);
            ctx.ExchangeRates.Add(new TipsCalculator.Data.Entitites.ExchangeRateEntity
            {
                FromCurrency = "usd",
                Id = 1, 
                Rate = (decimal) 1.23,
                ToCurrency = "eur"
            });
            // Saves correctly
            Assert.True(ctx.SaveChanges() > 0);
            var temp = (from p in ctx.ExchangeRates select p).ToList();
            Assert.Equal(1, temp.Count);
        }
    }
}
